/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.yaroslav_555.checkerhemmingcode;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Ярослав
 */
public class CheckerHemmingCode {

    public static final int length = 7;
    
    public static HashMap<Integer, Formula> formuls = new HashMap<>();
    
    public static void preInit(){
        formuls.put(0, new Formula(0, 1, 3));
        formuls.put(1, new Formula(0, 2, 3));
        formuls.put(3, new Formula(1, 2, 3));
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        preInit();
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Hello! Enter the bits!");
        
        String bytes = sc.nextLine();

        int[] array = new int[]{
            bytes.charAt(2) - 48,
            bytes.charAt(4) - 48,
            bytes.charAt(5) - 48,
            bytes.charAt(6) - 48
        };
        
        int[] regeneratedArray = generateHemmingCode(array);
        int traitor = 0;
        for(int i = 0; i < length; i++){
            if(regeneratedArray[i] != bytes.charAt(i) - 48 ) traitor += i+1;
        }
        
        if(traitor == 0){
            System.out.println("Looking good!");
        } else {
            System.out.println("Oh no! There is not the right bit! He's numbered: " + traitor);
        }
        
    }
    
    
    public static int[] generateHemmingCode(int[] array){
        int[] finalVariable = new int[CheckerHemmingCode.length];
        
        for(int i = 0, counter = 0; i<CheckerHemmingCode.length; i++){
            Formula formula = formuls.get(i);
            if(formula == null){
                finalVariable[i] = array[counter++];
                continue;
            }
            int preSum = 0;
            for(int j = 0; j<formula.bytes.length; j++) 
                preSum += array[formula.bytes[j]];
            finalVariable[i] = preSum % 2;
        }
        
        return finalVariable;
    }

    private static class Formula{
        int[] bytes;

        public Formula(int... bytes) {
            this.bytes = bytes;
        }   
    }
}
